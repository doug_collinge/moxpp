#ifndef MOXIE_H
#define MOXIE_H

#include <Arduino.h>

#define MOXDELAYTYPE unsigned long

class Mox {
public:
  // The non-static elements of Mox are used to implement a one-way
  // linked list, which starts at static moxEventList. The first item
  // on the list represents the next-to-occur timer event. "delay" is the
  // time in timer ticks between the previous timer event and the next one.
  // When the timer interrupt actually occurs virtual "action()" is called
  // to execute whatever needs to be done at that time.
  // Before the "action()" is executed the specialized Mox object is
  // removed from the event list and becomes "this" for "action()".
  // If it needs to be managed in some way (e.g. moved onto a free list)
  // then the "action()" code is responsible for that behaviour.
  // In many cases, the object will be permanently stack-allocated and so
  // not need to be managed.

  // Public constructor doesn't actually do anything.
  // TODO: review what this could usefully do.
  Mox() {};

  // A reference to the next object on the list, type not known.
  // Value is NULL if there is no next event.
  Mox *nextEvent;

  // Delay in timer ticks between this event and the previous one.
  MOXDELAYTYPE delay;

  // What to do when the event occurs; i.e., timer interrupt happens.
  virtual void action() = 0;

  // The head of the list of events, NULL if there are none.
  static Mox *moxEventList;

  // T1 timer clock prescaler divisor codes. The clock rate is set
  // by dividing the CPU clock ("F_CPU") by the divisor shown.
  enum T1Prescaler: uint8_t {
    T1PrescalerOff = 0,   // Timer clock disabled.
    T1Prescaler1 = 1,     //     16MHz, .0625us    4.096ms max
    T1Prescaler8 = 2,     //      2MHz,    .5us   32.768ms max
    T1Prescaler64 = 3,    //    250kHz,     4us  262.144ms max
    T1Prescaler256 = 4,   //   62.5kHz,    16us  1.048576s max
    T1Prescaler1024 = 5   // 15.625kHz,    64us, 4.194304s max
  };

  // Timer1 prescaler divisors. Index by the divisor selector above to
  // get the actual divisor value, useful in time calculations.
  static const uint16_t T1PrescaleDivisors[];

  // Initialized the Moxie system, including Timer1 hardware.
  static void initialize(T1Prescaler ps);

  // Start timing, execute any events scheduled at time zero.
  static void start();

  // What to call when the timer interrupts. Surprisingly, this is public.
  // That is because the ISR() needs access to it and the ISR cannot
  // (apparently) be defined inside the class.
  // TODO: try to solve this issue in a better way.
  static void timerEvent();

private:
  // Once the first event, for which the timer interrupted, has been
  // executed, now use this function to deal with any others that may
  // need to be executed, then reset the timer.
  static void processEventList();

public:
  // Schedule event "event" to have his "action()" function called
  // after "delay" timer ticks.
  static void cause(MOXDELAYTYPE delay, Mox *event);

  // Determine if there are any more events pending at the moment.
  static bool hasEvents() { return moxEventList != NULL; };


  // Utility queue management routines for the benefit of specialized
  // classes. Not used in the base class, Mox, which has no free list.
  static void push(Mox* &head, Mox *event) {
    event->nextEvent = head;
    head = event;
  }

  static Mox* pop(Mox* &head) {
    if(head != NULL) {
      Mox *item = head;
      head = head->nextEvent;
      item->nextEvent = NULL;
      return item;
    }
    return NULL;
  }

  static bool hasEntries(Mox* &head) {
    return head != NULL;
  }

private:
  // Whatever the prescaler was last set to.
  static T1Prescaler prescale;
public:
  // Return whatever the prescaler was initialized to.
  T1Prescaler getPrescale() __attribute__((always_inline)) { return prescale; }

private:
  // Bit patterns for Timer/Counter Control Register A and B for Timer1

  // Control Register A
  // "20.14.1 Normal port operation, OC1A/OC1B disconnected."
  static const uint8_t TCCR1A_b7to4 = (uint8_t)0x00;
  // Timer mode: "20.12.2 Clear Timer on Compare Match (CTC) Mode"
  static const uint8_t TCCR1A_b1to0 = (uint8_t)0x00;

  // Control Register B
  static const uint8_t TCCR1B_b4to3 = (uint8_t)0x08; // WGM13,12 = 0,1
  // 20.14.12. Timer/Counter 1 Interrupt Mask Register
  // Bit 1 – OCIEA: Output Compare A Match Interrupt Enable
  // When this bit is written to '1', and the I-flag in the Status Register
  // is set (interrupts globally enabled), the Timer/Counter Output
  // Compare A Match interrupt is enabled. The corresponding Interrupt Vector
  // is executed when the OCFA Flag, located in TIFR1, is set.
  static const uint8_t TIMSK1_bit1 = (uint8_t)0x02;

  static void initializeT1(T1Prescaler ps) __attribute__((always_inline)) {
    // Initialize Timer1 appropriately, which means:
    // - Disconnect the timer from the digital pins it uses.
    // - Throw the timer into CTC mode, using OC1A as limit register.
    // - Turn off the prescaler so the timer won't interrupt unexpectedly.
    // We are only going to call this once so we might as well inline it.

    // Remember the prescale for when we start() the timer.
    // We do not want the timer running until we start() it.
    prescale = ps;

    TCCR1A = TCCR1A_b7to4 | TCCR1A_b1to0;
    static const uint8_t TCCR1B_b2to0 = T1PrescalerOff; // CS12,11,10 = 0,0,0 = clock off
    TCCR1B = TCCR1B_b4to3 | TCCR1B_b2to0;
  }

  static void setT1Limit(uint16_t tix) __attribute__((always_inline)) {
    // Set the limit value of the counter. In this mode,
    // TCNT1 counts up prescaled ticks until it matches OCR1A,
    // resets TCNT1 so that it willl be zero on the next cycle,
    // then interrupts and continues counting up.
    // This is only two instructions so inline it, too.
    OCR1A = tix;
  }

  static void startT1() __attribute__((always_inline)) {
    // Contigure Timer1 into the desired state:
    // - Counter at zero,
    // - Control registers selecting CTC mode with OCR1A as the limit register,
    // - Enable the OCIEA interrupt mask.
    // In this mode, the T1 counter counts up on each prescaled tick
    // until the counter matches what is in OCR1A. Then it resets the
    // counter to zero and interrupts, while continuing to count.
    // This is called only once so inline it.
    TCNT1 = 0;
    TCCR1A = TCCR1A_b7to4 | TCCR1A_b1to0;
    TCCR1B = TCCR1B_b4to3 | prescale;
    // Set OCIEA Output Compare A Match Interrupt mask bit.
    TIMSK1 |= TIMSK1_bit1;
  }

  static void stopT1() __attribute__((always_inline)) {
    // Prevent Timer1 from causing further interrupts.
    // I.e., configure off the interrupt flag.
    TIMSK1 &= ~TIMSK1_bit1;
  }

};

#endif // define MOXIE
