#include <Arduino.h>

void L( char const* s ) { Serial.println(s); }
void LD( char const* s, int d ) { Serial.print(s);Serial.print(":");Serial.println(d); }

#include <Moxpp.h>

// Interrupt Service Routine for Timer1 Comparison Match A.
ISR(TIMER1_COMPA_vect)
{
  // Timer1 has just reached equality with OCR1A and set the counter to zero.
  // It continues to count and we don't want it to hit the limit while
  // we process the action queue. So set the limit OCR1A to the max,
  // thus giving ourselves as long as possible.
  // We will reset it later when we know long we will have to wait.
  OCR1A = 0xffff;
  Mox::timerEvent();
}


// Required static instances.
const uint16_t Mox::T1PrescaleDivisors[6] = {0,1,8,64,256,1024};
Mox *Mox::moxEventList = NULL;
Mox::T1Prescaler Mox::prescale = Mox::T1PrescalerOff;

void Mox::initialize(T1Prescaler ps) {
  moxEventList = NULL;
    initializeT1(ps);
}

void Mox::start() {
  TCNT1 = 0;  // To keep track of real time we set the timer.
  setT1Limit(0xffff); // We don't want it to interrupt until we know when.
  startT1();  // Start the timer counting, real time and schedule time = zero.
  processEventList(); // Do any initial events.
}

void Mox::timerEvent() {
  // Timer has just gone off.
  // We have an event, something to do. Remove the first event from the list but
  // save a reference to it, execute it (which may update the list),
  // reset the timer if there is still anything on the list, go back
  // to background thread until the timer goes off again.
  // If there are no more events on the list there is nothing more to do
  // until someone puts something on the list and starts the timer.
  // It is possible that there are one or more additional events at the
  // same time as the one we just executed; i.e., with zero delay.
  // If so, we should execute them all without pointlessly resetting the timer.

  if( moxEventList != NULL ) {
    Mox* currentEvent = moxEventList;
    moxEventList = moxEventList->nextEvent;
    currentEvent->action(); // May insert something on moxEventList!

    processEventList();
  }
}

void Mox::processEventList() {
  // Process everything on the event list that needs doing right away
  // and set the timer when it is all taken care of.

  // Keep track of schedule time. The just-executed event was supposed
  // to happen just now, after a timer interrupt so we are on schedule so far.
  // This variable is to keep track of how much schedule time we have
  // consumed during this interrupt so far.
  MOXDELAYTYPE elapsedTicks = 0;

  while( moxEventList != NULL )
  {
    // We have at least one item on the event list that needs to be processed.
    // If the delay is zero we should just execute it right away.
    // Otherwise, we need to check to see if setting the clock makese sense.
    // If the clock has already passed the point where the action should
    // be executed then we'll just execute the action without setting the
    // clock. If not, then setting the clock is necessary, so do it.

    // The clock actually counts from zero to the limit value so we'll
    // subtract 1 to get the right interval.
    unsigned int dely = elapsedTicks + moxEventList->delay - 1;
    if( moxEventList->delay != 0 && dely > TCNT1 ) {
      // Event is in the future so set the timer, we will execute it then.
      setT1Limit(elapsedTicks + moxEventList->delay - 1);
      if( dely <= TCNT1 ) {
        // We caught the counter stepping behind our backs! So if it had
        // ticked over before we finished the write we may have written
        // the same value as the counter had, which on this timer
        // suppresses the interrupt!!! Can't have that, or we'd have to
        // wait a complete cycle for it to interrupt again. So if there is a
        // possibility that it happened we have to fix it by just executing
        // the action and going on to the next event, if any. It would
        // have happened immediately anyway.
        break;
      }

      break;
    }
    Mox* currentEvent = moxEventList;
    moxEventList = currentEvent->nextEvent;
    elapsedTicks += currentEvent->delay;
    // During the following action() the contents of the event list may
    // change but moxEventList will always be the head of the list and
    // represent the next action that needs to be peformed.
    // The state of the object pointed to by currentEvent may change,
    // and it may have been recycled and even reused during the action().
    currentEvent->action();
  }
}

void Mox::cause(MOXDELAYTYPE newdelay, Mox *newevent) {
  // Schedule a new event after newdelay.
  // There is a list of up-coming events, forward-chained using "nextEvent".
  // The first event on the list is pointed to by "moxEventList".
  // The delay until the next event in the list is in "delay".
  // To schedule the new event we need to find where in the list its
  // delay should put it with respect to the other events, insert it there,
  // and adjust its delay to account for the total of the delays of the
  // events on the list that will come before it. If should happen at
  // exactly the same time as one or more other events then put it after
  // the last one (since it came after) and set its delay to zero.
  //
  // There are three cases to deal with: a) there are no events on the list,
  // so the list becomes just the new event; b) the new event should go before
  // the first event on the list, which means we have to alter the list
  // pointer variable; and c) the new event goes somewhere after the first
  // event on the list, maybe the last.

  if( moxEventList == NULL ) {  // Case a:
    // There is no list at the moment so no other events to worry about.
    // Insert new event as the only element of the list.
    newevent->nextEvent = NULL;
    newevent->delay = newdelay;
    moxEventList = newevent;
  }
  else if ( newdelay < moxEventList->delay ) {  // Case b:
    // New event has a delay less than the first up-coming event,
    // so this new one should become the first.
    //
    moxEventList->delay -= newdelay; // Adjust delay on next event.
    newevent->delay = newdelay;
    newevent->nextEvent = moxEventList;
    moxEventList = newevent;
  }
  else { // Case c:
    // There is at least one event on the list and the new event must go
    // somewhere after it. So subtract the delay of the first event on the
    // list from the new event's delay, and compare the result with the
    // delay on the next event on the list. If the new event's delay is
    // strictly less than that, insert the new event there and stop.
    // If it is greater than or equal to that then advance to the next
    // event on the list and rinse, repeat. If there is no next event,
    // insert the new event at the end of the list.

    newevent->delay = newdelay;
    Mox *node = moxEventList;     // Start at the beginning.
    // MOXDELAYTYPE total = node->delay;
    Mox *nextNode;
    while( true ) {
      newevent->delay -= node->delay; // Might produce zero!
      nextNode = node->nextEvent;
      if( nextNode == NULL ) {
          // We hit the end of the list, which means the new event must go
          // at the end, after "node". So insert it there.
          //  newevent->delay = newdelay - total;
          node->nextEvent = newevent;
          newevent->nextEvent = NULL;
          break;
      }
      // We are not at the end of the list so we have a next node.
      if( newevent->delay < nextNode->delay ) {
        // The new event should be inserted between "node" and "nextNode".
        // newevent->delay = newdelay - total;
        nextNode->delay -= newevent->delay;   // Result must be >0
        node->nextEvent = newevent;
        newevent->nextEvent = nextNode;
        break;
      }
      node = nextNode;
      // total += node->delay;
    }
  }
  // At this point the list is up to date, represents the up-coming events.
  // There is at least one event on the list because we just added one.
}
