// Flash a lot of lights and run a time-of-day clock at the same time.

#include <Arduino.h>
#include <Moxpp.h>

class PinControl : public Mox {
public:
  uint8_t pin;
  bool isBusy;

  PinControl() { }

  void init(uint8_t pn) {
    pin = pn;
    pinMode(pn, OUTPUT);
    isBusy = false;
  }

  void highFor(MOXDELAYTYPE tix) {
    if(isBusy) return;
    isBusy = true;
    digitalWrite(pin, HIGH);
    cause(tix,this);
  }

  void action() {
    digitalWrite(pin,LOW);
    isBusy = false;
  }
};

const int Npins = 9;
int pins[] = {5,6,7,8,9,10,11,12,13};
int periods[] = {100,101,102,103,104,105,106,107,108};

PinControl pinControllers[Npins];

class PinOnLoop : public Mox {
public:
  static Mox* freelist;
  static PinOnLoop* pop() { return (PinOnLoop*)Mox::pop(freelist); }

    uint8_t pinIndex;
    MOXDELAYTYPE period;
    int counter;

    PinOnLoop() { };

    static PinOnLoop* get(MOXDELAYTYPE p, int c, uint8_t ipn) {
      PinOnLoop* pol = pop();
      pol->period = p;
      pol->counter = c;
      pol->pinIndex = ipn;
      return pol;
    }

    void action() {
      pinControllers[pinIndex].highFor(period/2);
      counter--;
      if(counter>0) {
        Mox::cause(period,this);
      }
      else { // Finished looping, return this object to free list.
        Mox::push(freelist,this);
      }
    }

    static void init(int nevents) {
      freelist = NULL;
      for(int i=0;i<nevents;i++){
          push(freelist, new PinOnLoop());
      }
    }
};

Mox* PinOnLoop::freelist;
Mox::T1Prescaler presc = Mox::T1Prescaler1024;
long int pdiv = Mox::T1PrescaleDivisors[presc];

MOXDELAYTYPE LoopTime = 60UL * (F_CPU/pdiv);

class LongLoop : public Mox {
public:
  LongLoop() {
  }

  void action() {
    for(int ipin=0;ipin<Npins;ipin++) {
      PinOnLoop* pol = PinOnLoop::get(LoopTime/periods[ipin],periods[ipin]+1,ipin);
      Mox::cause(0UL, pol);
    }
    Mox::cause(LoopTime+2UL* F_CPU/pdiv,this);
  }
};

#include <stdio.h>
class TickTock : public Mox {
public:
  TickTock() { }
  uint8_t ticks;
  void action() {
    ticks++;
    cause(1UL*F_CPU/1024,this); //TODO: parametrize prescaler
  }
  int seconds, minutes, hours;
  bool updated;

  void reset() {
    ticks = 0;
    seconds = 0;
    minutes = 0;
    hours = 0;
    updated= true;  // Trick to get zero hhmmss to display.
    cause(1UL*F_CPU/1024,this); //TODO: parametrize prescaler
  }
  int getTicks() { return ticks; }

  void background() {
    cli();
    if(ticks>0) {
      updated = true;
      seconds += ticks;
      ticks = 0;
    }
    sei();
    if(updated) {
      while(seconds >= 60) {
        seconds -= 60; minutes++;
      }
      while(minutes>=60) {
        minutes -= 60; hours++;
      }
      char s[2+1+2+1+2]; // hh:mm:ss
      sprintf(s, "%02d:%02d:%02d", hours,minutes,seconds);
      Serial.println(s);
      updated = false;
    }
  }

};

unsigned long loopcount = 0UL;
LongLoop longloop;
int bgcount;

TickTock ticker;

void setup() {
  Serial.begin(57600);
  bgcount = 0;
  for(int i = 0;i<Npins;i++) {
    pinControllers[i].init(pins[i]);
  }

  PinOnLoop::init(Npins*2);

  Mox::initialize(Mox::T1Prescaler1024);
  Mox::cause(3UL* F_CPU/pdiv,&longloop);
  ticker.reset(); // Causes tick loop to start after 1 second.
  Mox::start();
}

void loop() {
  ticker.background();
  // Do whatever else needs doing in small slices here.
}
