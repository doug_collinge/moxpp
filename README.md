# README #

This is a library that is used to multiplex the 16-bit counter in an Atmel ucontroller, 
like what is in an Arduino. You can have as many virtual counters as you like, 
subject to the limitations of the processor speed. Time spend in handling the interrupts
is accounted for by the software so that time is accurately kept, as closely as possible.
If it gets behind it will catch up as fast as it can.

A suggested use-case is electronic music, where many simultaneous and often independent
process proceed simultaneously. Other complex control applications are also applicable.

This library is a work in progress. If anyone wants to use it, I am willing to help. 
Let me know.